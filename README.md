# Setup #

## Pré-requisitos ##
* É necessário ter o node e o npm instalado na maquina, estou usando a versão 0.10.32 do node e 1.4.28 do npm.
* É necessário ter o bower instalado como modulo global.

### Passo a Passo ###
* Rodar o comando npm install na pasta raiz do projeto.
* Rodar o comando npm start na pasta raiz do projeto.
* O seu browser padrão deve abrir o index da aplicação, caso isso não aconteça ela vai estar disponível na porta 3000.