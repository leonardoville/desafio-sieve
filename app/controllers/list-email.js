(function() {
	'use strict';

	angular
		.module('app')
		.controller('ListEmailController', ListEmailController)

		ListEmailController.$inject = ['ListEmailService'];

		function ListEmailController(ListEmailService) {
			var vm = this;
			vm.emails = [];

			setInitialState();
			getEmails(1);

			function setInitialState() {
				vm.search = '';
				vm.sortField = 'dataDeEnvio';
				vm.onlyTodayEmails = false
				vm.reverse = false;
			}

			function getEmails(page) {
				function success(reqObj) {
					reqObj.data.forEach(function(item) {
						if (item.dataDeEnvio)
							item.dataDeEnvio = new Date(item.dataDeEnvio);
					});

					vm.emails = reqObj.data;
					vm.totalItensInPage = vm.emails.length;
				}

				function error(err) {
					console.log('Error: ', err);
				}

				ListEmailService.getEmails(page).then(success, error);
			}

			vm.isSortUp = function(fieldName){
				return vm.sortField === fieldName && !vm.reverse;
			};

			vm.isSortDown = function(fieldName){
				return vm.sortField === fieldName && vm.reverse;
			};

			vm.sort = function(fieldName) {
				if (vm.sortField === fieldName) {
					vm.reverse = !vm.reverse;
				} else {
					vm.sortField = fieldName;
					vm.reverse = false;
				};
			}

			vm.isToday = function(date) {
				var todayDate = new Date().setHours(0, 0, 0, 0);

				var copyDate = angular.copy(date);
				copyDate = copyDate.setHours(0, 0, 0, 0);

				return copyDate === todayDate;
			};

			vm.pageChanged = function() {
				var nextPage = vm.currentPage;
				getEmails(nextPage);

				setInitialState();
			};

			vm.showOnlyTodayEmails = {
				originalEmails: [],
				setOriginalEmails: function() {
					this.originalEmails = angular.copy(vm.emails);
				},
				show: function() {
					if (!vm.onlyTodayEmails) {
						vm.emails = this.originalEmails;
						this.originalEmails = [];

						return;
					}

					this.setOriginalEmails();
					vm.emails = vm.emails.filter(function(email) {
						return vm.isToday(email.dataDeEnvio);
					});
				}
			};

			vm.filterEmails = function(email) {
				function emailContainsSearchText(email) {
					function matchSearchByField(field) {
						return email[field].search(vm.search) !== -1;
					}

					return matchSearchByField('nome') !== false || matchSearchByField('assunto') !== false;
				}

				if (vm.search === '') return email;
				else if (emailContainsSearchText(email)) return email;
			};
		}
})();