(function() {
	angular
		.module('app')
		.service('ListEmailService', ListEmailService);

	ListEmailService.$inject = ['$http'];

	function ListEmailService($http) {

		this.getEmails = function(page) {
			var REQ = {
				url: 'http://private-1dd4-desafiosieve.apiary-mock.com/emails/',
				method: 'GET'
			};

			REQ.url += page;

			return $http(REQ);
		};
	}

})();