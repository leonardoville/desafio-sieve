var gulp = require('gulp');
var webserver = require('gulp-webserver');
var bower = require('gulp-bower');
 
gulp.task('server', function() {
  gulp.src('./')
    .pipe(webserver({
      livereload: true,
      fallback: 'index.html',
      open: true,
	  port: 3000
    }));
});

gulp.task('bowerBuild', function() {
   return bower();
});